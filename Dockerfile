FROM rust:bookworm as builder

# build dependencies
RUN cargo install trunk
RUN USER=root cargo new --bin weinkeller
WORKDIR ./weinkeller
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock
RUN cargo build --release
RUN rm src/*.rs

# build the server
ADD . ./
# and build
RUN rm ./target/release/deps/weinkeller*
RUN cargo build --release

# build the wasm ui
RUN rustup target add wasm32-unknown-unknown
COPY ./weinkeller-sycamore ./weinkeller/weinkeller-sycamore
WORKDIR ./weinkeller/weinkeller-sycamore
RUN trunk build --release

FROM debian:bookworm-slim

ARG APP=/weinkeller

RUN apt-get update \
    && apt-get install -y ca-certificates libpq-dev tzdata \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 8000

ENV TZ=Etc/UTC \
    APP_USER=appuser

RUN groupadd $APP_USER \
    && useradd -g $APP_USER $APP_USER \
    && mkdir -p ${APP} \
    && mkdir -p ${APP}/weinkeller-sycamore

# copy server
COPY --from=builder /weinkeller/target/release/weinkeller ${APP}/weinkeller
# and wasm app dist folder
COPY --from=builder /weinkeller/weinkeller-sycamore/dist ${APP}/weinkeller-sycamore/dist

RUN chown -R $APP_USER:$APP_USER ${APP}

USER $APP_USER
WORKDIR ${APP}

CMD ["./weinkeller"]
