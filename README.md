# Weinkeller

The beginnings of a wine catalog

Five entities (at least thus far):
* Wineries - someone that makes/sells wine
* Bottles - a bottle of wine
* Tags - any bit of info about the bottle. Currently seeded with loose 'type' data (Red, White, Rosé, Sparkling).
* Varietals - a type of wine grape.  Really just a tag, but the BottleVarietals relation to Bottles stores an optional percentage to track the makeup of a given blend.
* BottleVarietals - many-to-many between a Bottle and it's component Varietals
* CellarSlots - map out the cellar, and reference bottles.  A Bottle might be represented more than once if we have more than one of the same winery/vintage/varietal mix.

Using:
* [Rust](https://www.rust-lang.org/)
* [Diesel](https://diesel.rs/)
* [Postgres](https://www.postgresql.org/)
* [Rocket](https://rocket.rs/)

There is a rust/wasm UI included here as submodule under `weinkeller-sycamore`.

## Building and Running

### Build the UI

```
pushd weinkeller-sycamore
trunk build --release
popd
```

### Configure the Server

Configuration lives in `.env` in the root of the project.
Needs to have two lines detailing connections to the same database

* `DATABASE_URL`: a standard Postgres connect URL, e.g.
```
postgres://user:pass@host/db
```
* `ROCKET_DATABASES`: nested hashes describing the same connect url, and naming it `wine`
```
'{wine={url="postgres://user:pass@host/db"}}'
```

The server _should_ run database migrations (found in `migrations` directory)
at startup to bring the db up-to-date,
but that is honestly the least-tested part of this.
If that does not happen, migrations can be run manually using
```
diesel migration run
```
(though you may need to `cargo install diesel_cli` in order to make the diesel executable available).

### Build the Server

```
cargo build --release
```

### Run the Server

```
cargo run --release
```

The server should now be running on the current host and port `8000`.
