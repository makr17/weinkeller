create table wineries (
  id uuid default gen_random_uuid() not null primary key,
  name varchar not null,
  address varchar,
  phone varchar,
  email varchar,
  url varchar,
  member boolean not null default false,
  created_at timestamptz not null default current_timestamp,
  updated_at timestamptz not null default current_timestamp
);

create unique index on wineries (name);

select diesel_manage_updated_at('wineries');


create table varietals (
  id uuid default gen_random_uuid() not null primary key,
  name varchar not null,
  created_at timestamptz not null default current_timestamp,
  updated_at timestamptz not null default current_timestamp
);

create unique index on varietals (name);

select diesel_manage_updated_at('varietals');


create table bottles (
  id uuid default gen_random_uuid() not null primary key,
  winery_id uuid not null references wineries (id),
  name varchar not null,
  vintage integer,
  barcode varchar,
  created_at timestamptz not null default current_timestamp,
  updated_at timestamptz not null default current_timestamp
);

-- todo: what is unique?  (winery_id, name, vintage)?

select diesel_manage_updated_at('bottles');


create table bottle_varietals (
  id uuid default gen_random_uuid() not null primary key,
  bottle_id uuid not null references bottles (id),
  varietal_id uuid not null references varietals (id),
  percentage integer,
  created_at timestamptz not null default current_timestamp,
  updated_at timestamptz not null default current_timestamp
);

create unique index on bottle_varietals (bottle_id, varietal_id);

select diesel_manage_updated_at('bottle_varietals');


create table cellar_slots (
  id uuid default gen_random_uuid() not null primary key,
  rank varchar not null default 'rack',
  x integer not null,
  y integer not null,
  bottle_id uuid not null references bottles (id),
  created_at timestamptz not null default current_timestamp,
  updated_at timestamptz not null default current_timestamp
);

create unique index on cellar_slots (rank, x, y);

select diesel_manage_updated_at('cellar_slots');
