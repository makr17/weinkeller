-- cribbed from https://www.cawineclub.com/wine-varietals
insert into varietals (name)
values ('Barbera'),
       ('Cabernet Sauvignon'),
       ('Chardonnay'),
       ('Chenin Blanc'),
       ('Dolcetto'),
       ('Fume Blanc'),
       ('Gewürztraminer'),
       ('Merlot'),
       ('Mourvedre'),
       ('Petite Sirah'),
       ('Pinot Grigio'),
       ('Pino Noir'),
       ('Riesling'),
       ('Sangiovese'),
       ('Sauvignon Blanc'),
       ('Syrah'),
       ('Viognier'),
       ('Zinfandel');

-- cribbed from https://www.cawineclub.com/wine-varietals
-- from the Blends section
insert into varietals (name)
values ('Cabernet Franc'),
       ('Petit Verdot'),
       ('Malbec'),
       ('Semillon'),
       ('Marsanne'),
       ('Roussanne'),
       ('Arbane'),
       ('Petit Meunier'),
       ('Petit Meslier'),
       ('Pinot Blanc');

-- not really _varietals_
-- this is really starting to become "tags"
-- but don't have a better place to put this at the moment
insert into varietals (name)
values ('Red'),
       ('Rosé'),
       ('Sparkling'),
       ('White');
