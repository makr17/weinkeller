create table tags (
  id uuid default gen_random_uuid() not null primary key,
  name varchar not null,
  created_at timestamptz not null default current_timestamp,
  updated_at timestamptz not null default current_timestamp
);

create unique index on tags (name);

select diesel_manage_updated_at('tags');

-- seed tags with common types
insert into tags (name) values ('Red'), ('White'), ('Rosé'), ('Sparkling');

-- and remove those values from varietals (they are not varietals)
delete from varietals where name in ('Red', 'White', 'Rosé', 'Sparkling');


create table bottle_tags (
  id uuid default gen_random_uuid() not null primary key,
  bottle_id uuid references bottles (id) not null,
  tag_id uuid references tags (id) not null,
  created_at timestamptz not null default current_timestamp,
  updated_at timestamptz not null default current_timestamp
);

create unique index on bottle_tags (bottle_id, tag_id);

select diesel_manage_updated_at('bottle_tags');
