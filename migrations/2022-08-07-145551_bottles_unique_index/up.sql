create unique index bottles_uniq_winery_name_vintage
on bottles (winery_id, name, vintage)
nulls not distinct;
