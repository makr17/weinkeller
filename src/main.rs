#[macro_use] extern crate rocket;

use std::collections::HashMap;

use diesel::prelude::*;
use diesel::dsl::sql_query;

//use rocket::{Rocket, Build};
//use rocket::fairing::AdHoc;
use rocket::fs::FileServer;
#[allow(unused_imports)]
use rocket::response::{Debug, status::Created, status::Accepted};
use rocket::serde::json::Json;
use rocket_sync_db_pools::{database, diesel};

use dotenv::dotenv;
use uuid::Uuid;

pub mod models;
pub mod schema;

use crate::models::*;

#[database("wine")]
struct Db(diesel::PgConnection);


// Full Bottles
// Mainly just convenience to return everything in one call

async fn _full_bottle_list(db: Db) -> Vec<FullBottle> {
    let query = "
select
  b.id
  , b.winery_id
  , b.name
  , b.vintage
  , b.barcode
  , b.created_at
  , b.updated_at
  , array_remove(array_agg(distinct cs.id), null) as cellar_slots
  , array_remove(array_agg(distinct bv.varietal_id), null) as varietals
  , array_remove(array_agg(distinct bt.tag_id), null) as tags
from
  bottles b
  left outer join cellar_slots cs on b.id = cs.bottle_id
  left outer join bottle_varietals bv on b.id = bv.bottle_id
  left outer join bottle_tags bt on b.id = bt.bottle_id
group by
  b.id
  , b.winery_id
  , b.name
  , b.vintage
  , b.barcode
  , b.created_at
  , b.updated_at
";
    let bottles: Vec<FullBottle> = db.run(move |conn| {
	sql_query(query)
	    .load(conn)
    }).await.expect("error executing full bottle query");

    bottles
}

#[get("/fullbottle")]
async fn list_full_bottle(db: Db) -> Json<Vec<FullBottle>> {
    let ret = _full_bottle_list(db).await;
    Json(ret)
}

#[get("/fullbottle/hash")]
async fn hash_full_bottle(db: Db) -> Json<HashMap<Uuid, FullBottle>> {
    let ret = _full_bottle_list(db).await.into_iter().map(|o| (o.id, o)).collect();
    Json(ret)
}

#[get("/fullbottle/<strid>")]
async fn read_full_bottle(db: Db, strid: String) -> Option<Json<FullBottle>> {
    // TODO: make From<uuid::Error> play with diesel::result::Error
    let id = Uuid::parse_str(&strid).unwrap();
    let query = "
select
  b.id
  , b.winery_id
  , b.name
  , b.vintage
  , b.barcode
  , b.created_at
  , b.updated_at
  , array_remove(array_agg(distinct cs.id), null) as cellar_slots
  , array_remove(array_agg(distinct bv.varietal_id), null) as varietals
  , array_remove(array_agg(distinct bt.tag_id), null) as tags
from
  bottles b
  left outer join cellar_slots cs on b.id = cs.bottle_id
  left outer join bottle_varietals bv on b.id = bv.bottle_id
  left outer join bottle_tags bt on b.id = bt.bottle_id
where
  b.id = $1
group by
  b.id
  , b.winery_id
  , b.name
  , b.vintage
  , b.barcode
  , b.created_at
  , b.updated_at
";
    let bottles: Vec<FullBottle> = db.run(move |conn| {
	sql_query(query)
	    .bind::<diesel::sql_types::Uuid, _>(id)
	    .get_results(conn)
    }).await.expect("failed to load full bottle");
    Some(Json(bottles[0].clone()))
}

#[post("/fullbottle", format="application/json", data="<newbottle>")]
async fn create_or_edit_full_bottle(db: Db, newbottle: Json<NewBottle>) ->  Json<FullBottle> {
    use crate::schema::bottles::dsl::*;

    let bottle = db.run(move |conn| {
        let bottle = conn.build_transaction()
            .run::<_, diesel::result::Error, _>(|conn| {
                // insert/update the bottle record
                let ib = newbottle.bottle();
                let bottle: Bottle = diesel::insert_into(bottles)
                    .values(&ib)
                    .on_conflict((winery_id, name, vintage))
                    .do_update()
                    .set(&ib)
                    .get_result(conn)?;

                // update cellar_slots as needed
                if let Some(val) = &newbottle.slots {
                    // update old slot(s) to null
                    let mut slots = val.clone();
                    let _ = diesel::update(
                        schema::cellar_slots::table
                            .filter(schema::cellar_slots::bottle_id.eq(&bottle.id))
                            .filter(schema::cellar_slots::id.ne_all(slots))
                    ).set(schema::cellar_slots::bottle_id.eq::<Option<Uuid>>(None))
                        .execute(conn)
                        .expect("failed to update old cellar slots");
                    // update new slots, setting bottle_id
                    slots = val.clone();
                    let _ = diesel::update(
                        schema::cellar_slots::table
                            .filter(schema::cellar_slots::id.eq_any(slots))
                    ).set(schema::cellar_slots::bottle_id.eq(&bottle.id))
                        .execute(conn)
                        .expect("failed to update new cellar slots");
                };

                // insert/delete bottle_varietals as needed
                if let Some(val) = &newbottle.varietals {
                    let vs = val.clone();
                    let _ = diesel::delete(
                        schema::bottle_varietals::table
                            .filter(schema::bottle_varietals::bottle_id.eq(&bottle.id))
                            .filter(schema::bottle_varietals::varietal_id.ne_all(vs.clone()))
                    ).execute(conn)
                        .expect("failed to delete old bottle varietals");
                    // batch the insert
                    let insertable = vs.iter()
                        .map(|vid| InsertableBottleVarietal {
                            bottle_id: bottle.id,
                            varietal_id: *vid,
                            percentage: None
                        })
                        .collect::<Vec<InsertableBottleVarietal>>();
                    let _ = diesel::insert_into(schema::bottle_varietals::table)
                        .values(insertable)
                        .on_conflict_do_nothing()
                        .execute(conn)
                        .expect("failed to insert new bottle varietal");
                }

                // insert/delete bottle_tags as needed
                if let Some(val) = &newbottle.tags {
                    let ts = val.clone();
                    let _ = diesel::delete(
                        schema::bottle_tags::table
                            .filter(schema::bottle_tags::bottle_id.eq(&bottle.id))
                            .filter(schema::bottle_tags::tag_id.ne_all(ts.clone()))
                    ).execute(conn)
                        .expect("failed to delete old bottle tags");
                    // batch the insert
                    let insertable = ts.iter()
                        .map(|tid| InsertableBottleTag {
                            bottle_id: bottle.id,
                            tag_id: *tid,
                        })
                        .collect::<Vec<InsertableBottleTag>>();
                    let _ = diesel::insert_into(schema::bottle_tags::table)
                        .values(insertable)
                        .on_conflict_do_nothing()
                        .execute(conn)
                        .expect("failed to insert new bottle tag");
                }

                Ok(bottle)
            }).unwrap();
            bottle
    }).await;

    // return FullBottle
    let created = read_full_bottle(db, bottle.id.to_string()).await
        .ok_or("failed to read back new bottle");
    created.unwrap()
}


// Bottles

#[get("/bottle/hash")]
async fn hash_bottle(db: Db) -> Json<HashMap<Uuid, Bottle>> {
    use crate::schema::bottles::dsl::*;

    let ret: Vec<Bottle> = db.run(move |conn| {
        bottles
            .load(conn)
    }).await.expect("error loading bottles");
    let map = ret.into_iter().map(|o| (o.id, o)).collect();

    Json(map)
}

#[get("/bottle")]
async fn list_bottle(db: Db) -> Json<Vec<Bottle>> {
    use crate::schema::bottles::dsl::*;

    let ret: Vec<Bottle> = db.run(move |conn| {
        bottles
            .load(conn)
    }).await.expect("error loading bottles");

    Json(ret)
}


// Bottle Tags

#[get("/bottle_tag")]
async fn list_bottle_tag(db: Db) -> Json<Vec<BottleTag>> {
    use crate::schema::bottle_tags::dsl::*;

    let ret: Vec<BottleTag> = db.run(move |conn| {
        bottle_tags
            .load(conn)
    }).await.expect("error loading bottle tags");

    Json(ret)
}

// bottle_tags managed in create_or_edit_full_bottle


// Bottle Varietals

#[get("/bottle_varietal")]
async fn list_bottle_varietal(db: Db) -> Json<Vec<BottleVarietal>> {
    use crate::schema::bottle_varietals::dsl::*;

    let ret: Vec<BottleVarietal> = db.run(move |conn| {
        bottle_varietals
            .load(conn)
    }).await.expect("error loading bottle varietals");

    Json(ret)
}

// bottle_varietals managed in create_or_edit_full_bottle


// Cellar Slots

#[get("/cellar_slot")]
async fn list_cellar_slot(db: Db) -> Json<Vec<CellarSlot>> {
    use crate::schema::cellar_slots::dsl::*;

    let ret: Vec<CellarSlot> = db.run(move |conn| {
        cellar_slots
            .load(conn)
    }).await.expect("error loading cellar slots");

    Json(ret)
}

#[get("/cellar_slot/open")]
async fn list_open_cellar_slot(db: Db) -> Json<Vec<CellarSlot>> {
    use crate::schema::cellar_slots::dsl::*;

    let ret: Vec<CellarSlot> = db.run(move |conn| {
        cellar_slots
            .filter(schema::cellar_slots::bottle_id.is_null())
	    .order(schema::cellar_slots::rank)
            .load(conn)
    }).await.expect("error loading cellar slots");

    Json(ret)
}


// Tags

#[get("/tag/hash")]
async fn hash_tag(db: Db) -> Json<HashMap<Uuid, Tag>> {
    use crate::schema::tags::dsl::*;

    let ret: Vec<Tag> = db.run(move |conn| {
        tags
            .load(conn)
    }).await.expect("error loading tags");
    let map = ret.into_iter().map(|o| (o.id, o)).collect();

    Json(map)
}

#[get("/tag")]
async fn list_tag(db: Db) -> Json<Vec<Tag>> {
    use crate::schema::tags::dsl::*;

    let ret: Vec<Tag> = db.run(move |conn| {
        tags
            .load(conn)
    }).await.expect("error loading tags");

    Json(ret)
}

#[post("/tag", format="application/json", data="<newtag>")]
async fn create_tag(db: Db, newtag: Json<InsertableTag>) ->  Json<Tag> {
    use crate::schema::tags::dsl::*;
    let tag: Tag = db.run(move |conn| {
        diesel::insert_into(tags)
            .values(&*newtag)
            .get_result(conn)
    }).await.expect("error inserting new tag");
    Json(tag)
}


// Varietals

#[get("/varietal/hash")]
async fn hash_varietal(db: Db) -> Json<HashMap<Uuid, Varietal>> {
    use crate::schema::varietals::dsl::*;

    let ret: Vec<Varietal> = db.run(move |conn| {
        varietals
            .load(conn)
    }).await.expect("error loading varietals");
    let map = ret.into_iter().map(|o| (o.id, o)).collect();

    Json(map)
}

#[get("/varietal")]
async fn list_varietal(db: Db) -> Json<Vec<Varietal>> {
    use crate::schema::varietals::dsl::*;

    let ret: Vec<Varietal> = db.run(move |conn| {
        varietals
            .load(conn)
    }).await.expect("error loading varietals");

    Json(ret)
}

#[post("/varietal", format="application/json", data="<newvarietal>")]
async fn create_varietal(db: Db, newvarietal: Json<InsertableVarietal>) ->  Json<Varietal> {
    use crate::schema::varietals::dsl::*;
    let varietal: Varietal = db.run(move |conn| {
        diesel::insert_into(varietals)
            .values(&*newvarietal)
            .get_result(conn)
    }).await.expect("error inserting new varietal");
    Json(varietal)
}


// Wineries

#[get("/winery/hash")]
async fn hash_winery(db: Db) -> Json<HashMap<Uuid, Winery>> {
    use crate::schema::wineries::dsl::*;

    let ret: Vec<Winery> = db.run(move |conn| {
        wineries
            .load(conn)
    }).await.expect("error loading wineries");
    let map = ret.into_iter().map(|o| (o.id, o)).collect();

    Json(map)
}

#[get("/winery")]
async fn list_winery(db: Db) -> Json<Vec<Winery>> {
    use crate::schema::wineries::dsl::*;

    let ret: Vec<Winery> = db.run(move |conn| {
        wineries
            .load(conn)
    }).await.expect("error loading wineries");

    Json(ret)
}

#[post("/winery", format="application/json", data="<newwinery>")]
async fn create_winery(db: Db, newwinery: Json<InsertableWinery>) ->  Json<Winery> {
    use crate::schema::wineries::dsl::*;
    let winery: Winery = db.run(move |conn| {
        diesel::insert_into(wineries)
            .values(&*newwinery)
            .get_result(conn)
    }).await.expect("error inserting new winery");
    Json(winery)
}


#[launch]
fn rocket() -> _ {
    dotenv().ok();
    rocket::build()
        .attach(Db::fairing())
//        .attach(AdHoc::on_ignite("Diesel Migrations", run_migrations))
        .mount("/", FileServer::from("weinkeller-sycamore/dist"))
	.mount(
	    "/api/v1",
	    routes![
                create_or_edit_full_bottle,
                create_tag,
                create_varietal,
                create_winery,
                hash_full_bottle,
                hash_bottle,
                hash_tag,
                hash_varietal,
                hash_winery,
                list_full_bottle,
		list_bottle,
                list_bottle_tag,
                list_bottle_varietal,
                list_cellar_slot,
                list_open_cellar_slot,
                list_tag,
                list_varietal,
                list_winery,
                read_full_bottle,
            ]
        )
}

/*
pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

async fn run_migrations(rocket: Rocket<Build>) -> Rocket<Build> {
    let conn = Db::get_one(&rocket).await.expect("database connection");
    conn.run_pending_migrations(MIGRATIONS).unwrap();
    rocket
}
*/
