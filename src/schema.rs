use diesel::prelude::*;

table! {
    bottle_tags (id) {
        id -> Uuid,
        bottle_id -> Uuid,
        tag_id -> Uuid,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    bottle_varietals (id) {
        id -> Uuid,
        bottle_id -> Uuid,
        varietal_id -> Uuid,
        percentage -> Nullable<Int4>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    bottles (id) {
        id -> Uuid,
        winery_id -> Uuid,
        name -> Varchar,
        vintage -> Nullable<Int4>,
        barcode -> Nullable<Varchar>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    cellar_slots (id) {
        id -> Uuid,
        rank -> Varchar,
        x -> Int4,
        y -> Int4,
        bottle_id -> Nullable<Uuid>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    tags (id) {
        id -> Uuid,
        name -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    varietals (id) {
        id -> Uuid,
        name -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    wineries (id) {
        id -> Uuid,
        name -> Varchar,
        address -> Nullable<Varchar>,
        phone -> Nullable<Varchar>,
        email -> Nullable<Varchar>,
        url -> Nullable<Varchar>,
        member -> Bool,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

joinable!(bottle_tags -> bottles (bottle_id));
joinable!(bottle_tags -> tags (tag_id));
joinable!(bottle_varietals -> bottles (bottle_id));
joinable!(bottle_varietals -> varietals (varietal_id));
joinable!(bottles -> wineries (winery_id));
joinable!(cellar_slots -> bottles (bottle_id));

allow_tables_to_appear_in_same_query!(
    bottle_tags,
    bottle_varietals,
    bottles,
    cellar_slots,
    tags,
    varietals,
    wineries,
);
